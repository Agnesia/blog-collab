<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use DB;
use Carbon\Carbon;
use App\Comment;
class CommentController extends Controller
{
    public function create() {
        return view('comments.show');
    }

    public function store(Request $request){
       // dd($request->all());
       $request->validate([
           'nama' => 'required'
       ]);

      /* $query = DB::table("comments")->insert([
           "nama" => $request["nama"],
           "isi" => $request ["isi"]
       ]);
       */
        //Eloquent
       $comment = new Comment;
       $comment->nama = $request["nama"];
       $comment->isi = $request["isi"];
      // $comment->create_at = $request["created_at"];
        
      

     
       
        $comment->save();
        //batas
        Alert::success('Succes !', 'Comment has added !!');
        return redirect('/comment');
       //return redirect('/comment')->with('success','Comment has added!');
    }
    //hitung comment
    
   

    public function index(){
        //$comments=DB::table("comments")->get();
        
        //dd($comments);

        //eloquent
        $comments= Comment::all();
        
        return view('comments.index' , compact('comments'));
    }

    public function edit($id) {
        //$comment = DB::table("comments")->where('id',$id)->first();
        //eloquent
        $comment = Comment::find($id);
        return view('comments.edit' , compact('comment'));
    }

    public function update($id, Request $request){
       /* $query = DB::table("comments")
                    ->where('id',$id)
                    ->update([
                        'isi' => $request['isi']
                    ]);
                    */

                    //eloquent
                    $update = Comment::where('id' , $id)->update([
                        "nama" => $request["nama"],
                        "isi" => $request["isi"],
                       // "created_at"=> $request["created_at"]
                    ]);
                    Alert::success('Success !', 'Comment has Updated !!');
                    return redirect('/comment');
                  // return redirect('/comment') ->with('success','Comment has updated!');

    }

    public function destroy($id){
        //$query = DB::table('comments')->where('id', $id)->delete();
        //eloquent
        Comment::destroy($id);
        Alert::success('Success !', 'Comment has Deleted!!');
        return redirect('/comment');
        //return redirect('/comment')->with('success', 'Comment has deleted!');
    }
}
