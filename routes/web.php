<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return view('master');
//});

//Route::get('/show', function () {
//    return view('show');
//});

//Route::get('/index', function () {
  //  return view('index');
//});


Route::get('/comments/show' , 'CommentController@create');

Route::post('/comments/show' , 'CommentController@store');

//menampilkan
Route::get('/comment', 'CommentController@index');

Route::get('/comment/{id}/edit' , 'CommentController@edit');

Route::put('/comment/{id}' , 'CommentController@update');

Route::delete('/comment/{id}' , 'CommentController@destroy');
