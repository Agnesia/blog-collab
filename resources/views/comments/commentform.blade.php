@extends ('master')

@section ('commentform')
    
<div class="comment-form">
    <h4>Leave a Comment</h4>
    <form role="form" action="/show" method="POST">
        @csrf
        <div class="form-group form-inline">
          <div class="form-group col-lg-6 col-md-12 name">
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'">
          </div>
         										
        </div>
        
        <div class="form-group">
            <textarea class="form-control mb-10" rows="5" name="isi" id="isi" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Comment'" required=""></textarea>
        </div>
        <a href="#" class="primary-btn text-uppercase">Post Comment</a>	
    </form>
</div>


@endsection